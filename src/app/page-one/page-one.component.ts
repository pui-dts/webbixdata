import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from './../api.service';
import { Observable , pipe} from 'rxjs';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}


@Component({
  selector: 'app-page-one',
  templateUrl: './page-one.component.html',
  styleUrls: ['./page-one.component.css']
})
export class PageOneComponent implements OnInit {
  
  constructor(private api: ApiService) { }
  interval: any;
  public userName;
  public image;
  ngOnInit() {
  this.interval = setInterval(() => {
        this.api.getAlluser().subscribe((data) => { 
          // this.user.push(data)
          this.userName = data[0].firstname;
          this.image = data[0].image;
         });
    }, 1000);
  }
  tiles: Tile[] = [
    {text: 'One', cols: 1, rows: 2, color: 'lightblue'},
    {text: 'Two', cols: 3, rows: 1, color: 'lightgreen'},
    {text: 'Three', cols: 3, rows: 1, color: 'lightpink'}
  ];
  
}

